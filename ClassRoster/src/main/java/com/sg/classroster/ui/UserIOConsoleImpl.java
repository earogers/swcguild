/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.classroster.ui;

import java.util.Scanner;

/**
 *
 * @author Elizabeth Rogers
 */
public class UserIOConsoleImpl implements UserIO {
    private Scanner sc = new Scanner(System.in);

    @Override
    public void print(String message) {
        System.out.println(message);
    }

    @Override
    public double readDouble(String prompt) {
        double userInput;
        
        do {
            System.out.print(prompt);
            String stringUserInput = sc.nextLine();
            try {
                userInput = Double.parseDouble(stringUserInput);
                break;
            } catch (NumberFormatException ex) {
                System.out.println("Sorry, that input isn't valid. Try again.");
            }
        } while (true);
        
        return userInput;
    }

    @Override
    public double readDouble(String prompt, double min, double max) {
        double userInput;
        
        do {
            System.out.print(prompt);
            String stringUserInput = sc.nextLine();
            try {
                userInput = Double.parseDouble(stringUserInput);
                if (userInput >= min && userInput <= max) {
                    break;
                } else {
                    System.out.println("Please enter between " + min + " and "
                            + max + ".");
                }            
            } catch (NumberFormatException ex) {
                System.out.println("Sorry, that input isn't valid. Try again.");
            }
        } while (true);
        
        return userInput;
    }

    @Override
    public float readFloat(String prompt) {
        float userInput;
        
        do {
            System.out.print(prompt);
            String stringUserInput = sc.nextLine();
            try {
                userInput = Float.parseFloat(stringUserInput);
                break;       
            } catch (NumberFormatException ex) {
                System.out.println("Sorry, that input isn't valid. Try again.");
            }
        } while (true);
        
        return userInput;
    }

    @Override
    public float readFloat(String prompt, float min, float max) {
        float userInput;
        
        do {
            System.out.print(prompt);
            String stringUserInput = sc.nextLine();
            try {
                userInput = Float.parseFloat(stringUserInput);
                if (userInput >= min && userInput <= max) {
                    break;
                } else {
                    System.out.println("Please enter between " + min + " and "
                            + max + ".");
                }            
            } catch (NumberFormatException ex) {
                System.out.println("Sorry, that input isn't valid. Try again.");
            }
        } while (true);
        
        return userInput;
    }

    @Override
    public int readInt(String prompt) {
        int userInput;
        
        do {
            System.out.print(prompt);
            String stringUserInput = sc.nextLine();
            try {
                userInput = Integer.parseInt(stringUserInput);
                break;       
            } catch (NumberFormatException ex) {
                System.out.println("Sorry, that input isn't valid. Try again.");
            }
        } while (true);
        
        return userInput;
    }

    @Override
    public int readInt(String prompt, int min, int max) {
        int userInput;
        
        do {
            System.out.print(prompt);
            String stringUserInput = sc.nextLine();
            try {
                userInput = Integer.parseInt(stringUserInput);
                if (userInput >= min && userInput <= max) {
                    break;
                } else {
                    System.out.println("Please enter between " + min + " and "
                            + max + ".");
                }            
            } catch (NumberFormatException ex) {
                System.out.println("Sorry, that input isn't valid. Try again.");
            }
        } while (true);
        
        return userInput;
    }

    @Override
    public long readLong(String prompt) {
        long userInput;
        
        do {
            System.out.print(prompt);
            String stringUserInput = sc.nextLine();
            try {
                userInput = Long.parseLong(stringUserInput);
                break;       
            } catch (NumberFormatException ex) {
                System.out.println("Sorry, that input isn't valid. Try again.");
            }
        } while (true);
        
        return userInput;
    }

    @Override
    public long readLong(String prompt, long min, long max) {
        long userInput;
        
        do {
            System.out.print(prompt);
            String stringUserInput = sc.nextLine();
            try {
                userInput = Long.parseLong(stringUserInput);
                if (userInput >= min && userInput <= max) {
                    break;
                } else {
                    System.out.println("Please enter between " + min + " and "
                            + max + ".");
                }            
            } catch (NumberFormatException ex) {
                System.out.println("Sorry, that input isn't valid. Try again.");
            }
        } while (true);
        
        return userInput;
    }

    @Override
    public String readString(String prompt) {
        System.out.print(prompt);
        String userInput = sc.nextLine();
        return userInput;
    }
    
}
